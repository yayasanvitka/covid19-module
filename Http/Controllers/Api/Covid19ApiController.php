<?php

namespace Modules\Covid\Http\Controllers\Api;

use Modules\Covid\Facades\Covid;

class Covid19ApiController
{
    public $filename;
    
    public function index()
    {
        return response()->json(Covid::get(), 200);
    }
    
    public function listCountry()
    {
        return response()->json(Covid::get()->pluck('country'), 200);
    }
    
    public function byCountry($country)
    {
        $data = Covid::country($country)->get();
        $data->last_update = $data->last_update->timezone('UTC')->format('Y-m-d'.'\T'.'H:i:s'."+07:00");
        return response()->json($data, 200);
    }
}
