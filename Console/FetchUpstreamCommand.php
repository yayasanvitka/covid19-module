<?php

namespace Modules\Covid\Console;

use Illuminate\Console\Command;
use Modules\Covid\Facades\Covid;

class FetchUpstreamCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'covid19-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data from upstream.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Getting new data..");
        Covid::fetch();
        $this->info("Done!");
    }
}
