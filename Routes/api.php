<?php

Route::group([
    'middleware' => 'throttle:120,1',
    'prefix' => 'api/covid19',
    'namespace' => 'Api'
], function () {
    Route::get('/', 'Covid19ApiController@index');
    Route::get('countries', 'Covid19ApiController@listCountry');
    Route::get('countries/{country}', 'Covid19ApiController@byCountry');
    
});
