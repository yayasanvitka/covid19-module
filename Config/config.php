<?php
return [
    'endpoint' => env('COVID_ENDPOINT', 'https://siakad.iteba.ac.id/api/covid19')
];
