# COVID-19 API
[![Latest Stable Version](https://img.shields.io/packagist/v/yayasanvitka/covid19-module.svg)](https://packagist.org/packages/yayasanvitka/covid19-module)
[![Total Downloads](https://img.shields.io/packagist/dt/yayasanvitka/covid19-module.svg)](https://packagist.org/packages/yayasanvitka/covid19-module)
[![License](https://img.shields.io/packagist/l/yayasanvitka/covid19-module.svg)](https://packagist.org/packages/yayasanvitka/covid19-module)

## Introduction
`yayasanvitka/covid19-module` is a Laravel package which was created to easily publish and consume the API for live information about Novel Coronavirus-19.
This module is based on API from [javieraviles/covidAPI](https://github.com/javieraviles/covidAPI). 

## Documentation
### Requirement
This module is created as [laravel-modules](https://nwidart.com/laravel-modules/v4/introduction), and require the following:

- [Laravel](https://laravel.com) > 5.8.*
- [laravel-modules](https://nwidart.com/laravel-modules/v4/introduction) >= 5.1

### Installation & Setup
<code>composer require yayasanvitka/covid19-module</code>

### Web Route
<ul>
<li>/api/covid19: Global Data</li>
<li>/api/covid19/countries: List of countries</li>
<li>/api/covid19/countries/{COUNTRY}: Data per Country</li>
</ul>

### Facade Methods

- <code>Covid::fetch()</code>
<br />Fetch data from endpoint

- <code>Covid::get()</code>
<br />Get The Data. Returns *\Illuminate\Support\Collection*. Can be chained with *country()* method.
- <code>Covid::country()</code>
<br />Get the country data.
- <code>Covid::total()->cases</code>
<br /> Get total data (Global) for cases 
- <code>Covid::total()->todayCases</code>
<br /> Get total data (Global) for todayCases 
- <code>Covid::total()->todayDeath</code>
<br /> Get total data (Global) for todayDeath 
- <code>Covid::total()->recovered</code>
<br /> Get total data (Global) for recovered 
- <code>Covid::total()->deaths</code>
<br /> Get total data (Global) for deaths 
- <code>Covid::total()->active</code>
<br /> Get total data (Global) for active 
- <code>Covid::total()->critical</code>
<br /> Get total data (Global) for critical 
- <code>Covid::total()->casesPerOneMillion</code>
<br /> Get total data (Global) for casesPerOneMillion 

### Facade Methods
<code>php artisan covid19-update</code>
<br />Run this to update the database

## License
MIT License 2020, IT Yayasan Vitka.
Based on API from [javieraviles](https://github.com/javieraviles/covidAPI), the data may not be used for commercial purposes.
