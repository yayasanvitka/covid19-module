<?php

namespace Modules\Covid;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class Covid
{
    public $data;
    public $controller;
    public $total;
    protected $filename;
    
    public function __construct()
    {
        $this->filename = 'covid19.json';
        
        $data = collect($this->readFile());
        
        $this->data = collect();
        
        foreach ($data as $value) {
            $rData = (object) $value;
            $rData->last_update = Carbon::createFromTimestampUTC(substr($value['last_update'], 0, 10));
            $this->data = $this->data->push($rData);
        }
        
        $this->total();
    }
    
    public function total()
    {
        $this->total = new Total($this->data);
        return $this->total;
    }
    
    public function fetch()
    {
        $getdata = new GetData();
        return $getdata->fetch();
    }
    
    public function get()
    {
        if ($this->data->count() == 1) {
            return $this->data->first();
        }
        return $this->data;
    }
    
    public function country($countryName)
    {
        $this->data = $this->data->filter(function ($value, $key) use ($countryName) {
            if (strtolower($value->country) == strtolower($countryName)) {
                return $value;
            }
        });
        
        return $this;
    }
    
    private function readFile()
    {
        if (!Storage::disk('local')->has($this->filename)) {
            return $this->fetch();
        } else {
            return collect(json_decode(Storage::disk('local')->get($this->filename), true));
        }
    }
    
}
