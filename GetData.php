<?php

namespace Modules\Covid;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class GetData
{
    protected $filename;
    
    public function __construct()
    {
        $this->filename = 'covid19.json';
    }
    
    public function fetch()
    {
        $client = new Client();
        $last_update = now()->timezone('Asia/Jakarta')->timestamp;
        
        try {
            $result = $client->request("GET", config('covid.endpoint'), [
                'headers' => [
                    "Accept" => "*/*",
                    "Content-Type" => "application/json"
                ]
            ]);
        } catch (ClientException $exception) {
            return ['error' => true, 'message' => $exception->getMessage()];
        }
        
        if ($result->getStatusCode() == 200) {
            $data = collect(json_decode($result->getBody(), true));
            try {
                foreach ($data as $key=>$val) {
                    $val['last_update'] = $last_update;
                    $data[$key] = $val;
                }
                
                $this->store($data);
            } catch (\Exception $exception) {
                return ['error' => true, 'message' => $exception->getMessage()];
            }
        }
        
        return $data;
    }
    
    private function store(Collection $data)
    {
        Storage::disk('local')->put($this->filename, $data->toJson());
        return true;
    }
}
