<?php

namespace Modules\Covid;

use Illuminate\Support\Collection;

class Total
{
    protected $data;
    
    public $cases;
    public $todayCases;
    public $todayDeath;
    public $recovered;
    public $deaths;
    public $active;
    public $critical;
    public $casesPerOneMillion;
    
    public function __construct(Collection $data)
    {
        $this->data = $data;
        
        $this->cases = $data->sum('cases');
        $this->todayCases = $data->sum('todayCases');
        $this->todayDeath = $data->sum('todayDeath');
        $this->recovered = $data->sum('recovered');
        $this->deaths = $data->sum('deaths');
        $this->active = $data->sum('active');
        $this->critical = $data->sum('critical');
        $this->casesPerOneMillion = $data->sum('casesPerOneMillion');
        
        return $this;
    }
}
