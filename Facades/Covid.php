<?php

namespace Modules\Covid\Facades;

use Illuminate\Support\Facades\Facade;

class Covid extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'covid';
    }
}
